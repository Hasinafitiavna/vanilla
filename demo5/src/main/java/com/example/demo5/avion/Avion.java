package com.example.demo5.avion;

import com.example.demo5.entities.Kilometrage;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;

@Data
@Entity
@Table(name="avion")

public class Avion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 45, name = "nom")
    private String nom;

//    @OneToMany(targetEntity = Kilometrage.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "idkilometrage", referencedColumnName = "id")
//    public ArrayList<Kilometrage> kilometrages;

    public Avion(String nom) {
        this.nom = nom;
    }

    public Avion() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
