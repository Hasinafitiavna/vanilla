package com.example.demo5.avion;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AvionRepository extends CrudRepository<Avion, Integer> {

}
