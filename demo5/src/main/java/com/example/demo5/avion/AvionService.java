package com.example.demo5.avion;

import java.util.List;

public interface AvionService {
    Avion saveAvion(Avion avion);
    List<Avion> getAllAvion();
    Avion getAvionById(int id);
    Avion updateAvion(Avion avion, int id);
    void deleteAvion(int id);
    UsefulEntity loginAvion(String nom,String pass);
}
