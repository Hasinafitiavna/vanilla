package com.example.demo5.avion;

import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
@CrossOrigin("*")
public class AvionServiceImpl implements AvionService {

    private AvionRepository avionRepository;

    @Autowired
    public AvionServiceImpl(AvionRepository avionRepository){
        super();
        this.avionRepository = avionRepository;
    }

    @Override
    public Avion saveAvion(Avion avion) {
        return avionRepository.save(avion);
    }

    @Override
    public List<Avion> getAllAvion(){
        return (List<Avion>) avionRepository.findAll();
    }

    @Override
    public Avion getAvionById(int id){
        return avionRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Vehicule", "Id", id));
    }

    @Override
    public Avion updateAvion(Avion avion, int id) {
        Avion existingAvion = avionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Avion", "Id", id));
        existingAvion.setNom(avion.getNom());

        avionRepository.save(existingAvion);
        return existingAvion;
    }

    @Override
    public void deleteAvion(int id) {
        avionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Vehicule", "Id", id));
        avionRepository.deleteById(id);
    }


    @Override
    public UsefulEntity loginAvion(String nom, String pass) {
        List<Avion> liste = getAllAvion();
        Boolean res = false;
        int id=0;
//        for(int i=0; i<liste.size(); i++){
//            if((liste.get(i).getNom().equals(nom))&&(liste.get(i).getPass().equals(pass))){
//                res = true;
//                id = liste.get(i).getId();
//                System.out.println("Successful login, vehicule name: "+liste.get(i).getNom() + ", id= " + liste.get(i).getId());
//                break;
//            }
//        }
        if(!res){
            System.out.println("you Vehicule name: "+nom+" do not exist in our database");
        }
        return new UsefulEntity(res,id);
    }
}
