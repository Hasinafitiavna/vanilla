package com.example.demo5.connexion;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnexionPostgresql {
    public static Connection getConn() throws Exception{
        String url="jdbc:postgresql://localhost:5432/tpvehicule";
        String user="postgres";
        String pass="stevensteven";
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(url,user,pass);
        return conn;
    }
}
