package com.example.demo5.connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class ConnexionSqlite {
    public Connection getConn() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:C:/Users/STEVEN/AppData/Local/Google/AndroidStudio2021.3/device-explorer/emulator-5554/data/data/com.example.myapplication3/databases/transaction.db";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}

