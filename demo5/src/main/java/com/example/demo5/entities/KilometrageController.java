package com.example.demo5.entities;


import com.example.demo5.avion.Avion;
import com.example.demo5.avion.AvionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/kilometrage")
@CrossOrigin("*")
public class KilometrageController {
    private KilometrageService kilometrageService;

    public KilometrageController(KilometrageService kilometrageService){
        super();
        this.kilometrageService=kilometrageService;
    }

    @GetMapping("{id}")
    public ArrayList<Kilometrage> getkm(@PathVariable("id") int id){
        return kilometrageService.getkm(id);
    }
}
