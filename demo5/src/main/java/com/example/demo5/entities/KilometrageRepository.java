package com.example.demo5.entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.ArrayList;

public interface KilometrageRepository extends JpaRepository<Kilometrage, Integer> {
    @Query(value="select k.id,k.idavion,k.km from avion join kilometrage k on avion.id = k.idavion where avion.id=?1",nativeQuery = true)
    public ArrayList<Kilometrage> gethis(int id);
}
