package com.example.demo5.entities;

import java.util.ArrayList;

public interface KilometrageService {
    ArrayList<Kilometrage> getkm(int id);
}
