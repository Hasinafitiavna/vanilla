package com.example.demo5.entities.pers;

import org.springframework.data.repository.CrudRepository;

public interface PersonnelRepository extends CrudRepository<Personnel, Integer> {

}
