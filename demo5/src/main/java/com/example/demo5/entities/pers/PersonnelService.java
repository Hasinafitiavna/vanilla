package com.example.demo5.entities.pers;

import com.example.demo5.avion.Avion;
import com.example.demo5.entities.pers.UsefulEntity;

import java.util.List;

public interface PersonnelService {
    UsefulEntity login(String nom, String pass);
    List<Personnel> getAllPersonnel();
}
