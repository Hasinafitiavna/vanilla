var xhr; 
try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
catch (e) 
{
    try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
    catch (e2) 
    {
        try {  xhr = new XMLHttpRequest();  }
        catch (e3) {  xhr = false;   }
    }
}

var xhr1;
try {  xhr1 = new ActiveXObject('Msxml2.XMLHTTP');   }
catch (e)
{
    try {   xhr1 = new ActiveXObject('Microsoft.XMLHTTP'); }
    catch (e2)
    {
        try {  xhr1 = new XMLHttpRequest();  }
        catch (e3) {  xhr1 = false;   }
    }
}

var str = window.location.href;
var url = new URL(str);
var pam = url.searchParams.get("id");

function getParameterByName(name)
{
 var results = name.exec(window.location.search);
 if(results == null)
 return ;
 else
 return decodeURIComponent(results[1]);
}

function getListeVehicules(){
    xhr.onreadystatechange  = function() 
    { 
       if(xhr.readyState  == 4){
        if(xhr.status  == 200) {
            var retour = JSON.parse(xhr.responseText);
            idv.innerHTML = "Id: "+retour['id'];
            marque.innerHTML = "Nom: "+retour['nom'];
        } else {
            document.dyn="Error code " + xhr.status;
        }
		}
    }; 
    xhr.open("GET", "http://localhost:8080/api/avions/"+pam,true);
    xhr.send(null);
}
getListeVehicules();
function getListeKilometrage(){
    xhr1.onreadystatechange  = function()
    {
        if(xhr1.readyState  == 4){
            if(xhr1.status  == 200) {
                var ret = JSON.parse(xhr1.responseText);
                km.innerHTML = "Kilometrage (unite: km): "+ret[0]['km'];
            } else {
                document.dyn="Error code " + xhr1.status;
            }
        }
    };
    xhr1.open("GET", "http://localhost:8080/api/kilometrage/"+pam,true);
    xhr1.send(null);
}
getListeKilometrage();