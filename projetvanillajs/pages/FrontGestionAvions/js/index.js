var xhr; 
try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
catch (e) 
{
    try {   xhr = new ActiveXObject('Microsoft.XMLHTTP'); }
    catch (e2) 
    {
        try {  xhr = new XMLHttpRequest();  }
        catch (e3) {  xhr = false;   }
    }
}

function getListeVehicules(){
    xhr.onreadystatechange  = function() 
    { 
       if(xhr.readyState  == 4){
        if(xhr.status  == 200) {

            let row_1 = document.createElement('tr');
            let heading_1 = document.createElement('th');
            heading_1.innerHTML = "id";
            let heading_2 = document.createElement('th');
            heading_2.innerHTML = "nom";
            let heading_3 = document.createElement('th');
            heading_3.innerHTML = "pass";

            row_1.appendChild(heading_1);
            row_1.appendChild(heading_2);
            row_1.appendChild(heading_3);
            thead.appendChild(row_1);

            var retour = JSON.parse(xhr.responseText);

            if(sessionStorage.getItem('idpers')==null){
                for (i=0; i<retour.length; i++) {
                    let ligne = document.createElement('tr');
                    let colonne1 = document.createElement('td');
                    colonne1.innerHTML = retour[i]['id'];
                    let colonne2 = document.createElement('td');
                    colonne2.innerHTML = retour[i]['nom'];
                    let colonne3 = document.createElement('td');
                    colonne3.innerHTML = "<a href='login.html'>detail</a>";
                    ligne.appendChild(colonne1);
                    ligne.appendChild(colonne2);
                    ligne.appendChild(colonne3);
                    tbody.appendChild(ligne);
                }
            }

            if(sessionStorage.getItem('idpers')!=null){
                for (i=0; i<retour.length; i++) {
                    let ligne = document.createElement('tr');
                    let colonne1 = document.createElement('td');
                    colonne1.innerHTML = retour[i]['id'];
                    let colonne2 = document.createElement('td');
                    colonne2.innerHTML = retour[i]['nom'];
                    let colonne3 = document.createElement('td');
                    colonne3.innerHTML = "<a href='detail_vehicule.html?id="+retour[i]['id']+"'>detail</a>";
                    ligne.appendChild(colonne1);
                    ligne.appendChild(colonne2);
                    ligne.appendChild(colonne3);
                    tbody.appendChild(ligne);
                }
            }

        } else {
            document.dyn="Error code " + xhr.status;
        }
		}
    }; 
    xhr.open("GET", "http://localhost:8080/api/avions",true);
    xhr.send(null);
}

getListeVehicules();